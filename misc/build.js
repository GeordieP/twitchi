var packager = require('electron-packager');

// Run this file in Node to build an executable for Twitchi
// shortcut is to run "npm run rel" in project root

var ignores = [
    'babel-core',
    'babel-loader',
    'babel-preset-es2015',
    'babel-preset-react',
    'gulp',
    'gulp-sass',
    'react',
    'react-dom',
    'react-redux',
    'react-router',
    'redux',
    'webpack',
    '/app/js/react',
    '/app/sass',
    '/misc',
    'webpack-config.js',
    'Gulpfile.js',
    '.gitignore',
    '/build',
    'README.md'
]

var opts = {
	name: "Twitchi",
	dir: ".",
	asar: false,
	platform: "win32",
	arch: "x64",
    ignore: ignores,
    overwrite: true,
    out: './build/',
    prune: true,
    icon: './twitchpurple.ico'
}

packager(opts, function(err, appPath) {
	if (err) throw err;

	console.log("Build complete @ " + appPath);
});
