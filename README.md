# Twitchi

By Geordie Powers
https://bitbucket.org/GeordieP || https://github.com/geordiep

## Set-up for development
- Clone repo and run `npm install` from the project root, then run `webpack` from the project root
- Run `npm start` in project root to start
- See 'building' in this readme for instructions on creating an executable


## Editing

Making changes to this project as someone who's not me might be a bit confusing at first, since it's a SASS styled React app compiled into a bundle running inside and making calls to its Electron window.
Hopefully I can at least point in the right direction.

### Editing front-end CSS:
- Run command `gulp` in project root, Make changes in `/app/sass` and save. Gulp will see the changes and compile to `/app/css/*.css`

### Editing front-end UI:
- index.html doesn't do much, mainly just used for loading JS files
- Most UI rendering and changing is handled by react and redux
- Make changes to react and redux code in `/app/js/react/**/*.js`
    - Run `webpack` to compile the files in `/app/js/react/` and their dependencies into `/app/js/bundle.js` (`--watch` is recommended to avoid running weback every time; it will watch for file changes and cache previous bundles)

### Editing Electron/Window/System interactions:
- Windowing and Electron main (app entry point) and related modules are in `/electron-window/`
- Livestreamer process interaction happens in `/electron-window/livestreamer.js`
- Twitch authentication is handled in `/electron-window/ipchandler.js` as it needs to open a new window
    - Other interactions with the Twitch API are done in the front-end (electron renderer process)


## Building
Read [this](https://github.com/electron-userland/electron-packager/blob/master/docs/api.md) and edit `/misc/build.js` to change build properties, then either run it with node from the project root, or run `npm run build` from the project root
