var livestreamer = {}

if (ipc === undefined) var ipc = require('electron').ipcRenderer
if (events === undefined) var events = require('events')
if (eventEmitter === undefined) var eventEmitter = new events.EventEmitter()

livestreamer.open_stream = (username) => {
    ipc.send('ls-open-stream', username)
}

livestreamer.close_stream = (username) => {

}

window.livestreamer = livestreamer
