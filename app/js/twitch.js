'use strict'

var twitch = {};

if (ipc === undefined) var ipc = require('electron').ipcRenderer
if (events === undefined) var events = require('events')
if (eventEmitter === undefined) var eventEmitter = new events.EventEmitter()

/*---
* Auth
---*/

const client_id = '668t6vmbj9pnd0fza0lfsoj9xwpl0ff'
const redirect_uri = 'http://localhost:46984/oauthCallback'
const auth_scopes = 'user_read'
const auth_url = 'https://api.twitch.tv/kraken/oauth2/authorize?response_type=token&client_id='+ client_id +'&redirect_uri='+ redirect_uri +'&scope='+ auth_scopes

let access_token = ''
let followed_streams_url = 'https://api.twitch.tv/kraken/streams/followed?oauth_token='

/*---
* Requests
---*/

/* Make HTTP request to url, pass recieved data to callback */
var make_req = (url, callback) => {
    var req = new XMLHttpRequest()

    req.open('GET', url, true)
    req.onload = () => {
        if (req.status >= 200 && req.status < 400)
            callback(req.responseText)
        else
            console.error('twitch API returned an error')
    }

    req.onerror = () => {
        console.error('error connecting to server')
    }

    req.send()
}

// - Helper functions to call twitch API endpoints

/* Make a request to the followed streams URL. Callback is usually a front-end function that parses JSON streamer data and updates the UI */
twitch.fetch_live_follows = (callback) => {
    make_req(followed_streams_url + access_token, callback)
}

twitch.fetch_authenticated_username = (callback) => {
    make_req('https://api.twitch.tv/kraken/user?oauth_token=' + access_token, callback)
}

/*---
* IPC Events
---*/

/* When the system (Electron window) sends us a new access token, check its validity here and assign it */
/* request a new token if we get an invalid one from configstore 
    (doing so will end up sending this same IPC message, so we process the new one the same way)
*/
ipc.on('auth-update-access-token', (event, token) => {
    // if the token we get is invalid, open a Twitch login window to get a new one
    if (token === null || token === undefined) {
        ipc.send('auth-refresh-access-token', auth_url)
    } else {
        access_token = token
        eventEmitter.emit('auth-access-token-refreshed')
    }
})

/*---
* Misc
---*/

/* Set everything up, invoke callback when finished and pass init success or failure bool */
twitch.init = (callback) => {
    if (access_token === '') {
        ipc.send('auth-fetch-access-token')
        eventEmitter.once('auth-access-token-refreshed', () => {
            // console.log("access token: " + access_token)
            callback(true)
        })
    } else {
        callback(true)
    }
}

twitch.logout = (callback) => {
    ipc.send('auth-refresh-access-token', auth_url)
}

// expose this class to other files via the browser window
window.twitch = twitch
