module.exports = {
    ActionTypes: {
        FETCH_USERNAME: 'FETCH_USERNAME',
        UPDATE_USERNAME: 'UPDATE_USERNAME',
        FETCH_FOLLOW_LIST: 'FETCH_FOLLOW_LIST',
        UPDATE_FOLLOW_LIST: 'UPDATE_FOLLOW_LIST',
        UPDATE_SETTING: 'UPDATE_SETTING'
    },
    DefaultUserState: {
        userName: ''
    },
    DefaultFollowedChannelsState: {
        live: [],
        reactFetching: false
    },
    DefaultSettingsState: {
        
    }
}
