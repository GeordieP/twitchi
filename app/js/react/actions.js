import * as constants from './constants'
// import * as twitch from '../twitch'
const twitch = window.twitch

export function FetchUsername(dispatch) {
    twitch.fetch_authenticated_username((data) => {
        dispatch(UpdateUsername(JSON.parse(data).display_name))
    })

    return {
        type: constants.ActionTypes.FETCH_USERNAME,
        reactFetching: true
    }
}

export function UpdateUsername(username) {
    return {
        type: constants.ActionTypes.UPDATE_USERNAME,
        username: username,
        reactFetching: false
    }
}

export function FetchFollowList(dispatch) {
    twitch.fetch_live_follows((data) => {
        let newList = []
        let streams = JSON.parse(data).streams

        for (let i = 0; i < streams.length; i++) {
            let stream = streams[i]

            newList.push({
                game: stream.game,
                viewers: stream.viewers,
                created_at: stream.created_at,
                thumbnail_url: stream.preview.medium + '?' + Math.random(),
                title: stream.channel.status,
                username: stream.channel.display_name,
                channel_url: stream.channel.url
            })
        }

        dispatch(UpdateFollowList(newList))
    })

    return {
        type: constants.ActionTypes.FETCH_FOLLOW_LIST,
        reactFetching: true
    }
}

export function UpdateFollowList(live) {
    return {
        type: constants.ActionTypes.UPDATE_FOLLOW_LIST,
        live: live,
        reactFetching: false
    }
}
