import * as constants from './constants'
import { combineReducers } from 'redux'

function user(state = constants.DefaultUserState, action) {
    switch(action.type) {
        case constants.ActionTypes.FETCH_USERNAME:
            return Object.assign({}, state, {
                reactFetching: action.reactFetching
            })
        break;
        case constants.ActionTypes.UPDATE_USERNAME:
            return Object.assign({}, state, {
                reactFetching: action.reactFetching,
                userName: action.userName
            })
        break;
        default:
            return state
    }
}

function followedChannels(state = constants.DefaultFollowedChannelsState, action) {
    switch(action.type) {
        case constants.ActionTypes.FETCH_FOLLOW_LIST:
            return Object.assign({}, state, {
                reactFetching: action.reactFetching
            })
        break;
        case constants.ActionTypes.UPDATE_FOLLOW_LIST:
            return Object.assign({}, state, {
                reactFetching: action.reactFetching,
                live: action.live
            })
        break;
        default:
            return state
    }
}

function settings(state, action) {
    return state
}

const TwitchiReducers = combineReducers({
    user,
    followedChannels,
    // settings
})

export default TwitchiReducers
