import React from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'

export default React.createClass({
    render() {
        return(
            <div>

                <div id="navBar">
                    <Link to="/">&lt; Home</Link>
                </div>
            
                <main>
                    <p>Settings</p>
                </main>
            </div>
        )
    }
})
