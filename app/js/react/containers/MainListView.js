import React from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import ChannelsList from '../components/ChannelsList'
import Channel  from '../components/Channel'
import { FetchFollowList } from '../actions'
let livestreamer = window.livestreamer
let twitch = window.twitch

let ListViewShape = React.createClass({
    render() {
        return (
            <div>
                <div id="navBar">
                    <Link to="/settings">Settings &gt;</Link>
                    <a href="#" onClick={(e) => this.props.refreshClick(e)}>Refresh</a>
                    <a href="#" onClick={(e) => this.props.logoutClick(e)}>Change Account</a>
                </div>
                <main>
                    {(() => {
                        if (this.props.reactFetching){
                            return <p>Updating...</p>
                        }
                        else{
                            return <ChannelsList liveChannels={this.props.liveChannels} openStreamClick={this.props.openStreamClick} />
                        }
                    })()}
                </main>
            </div>
        )
    }
})

const mapStateToProps = (state, ownProps) => {
    return {
        liveChannels: state.followedChannels.live,
        reactFetching: state.followedChannels.reactFetching
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        refreshClick: (e) => {
            e.preventDefault()
            dispatch(FetchFollowList(dispatch))
        },
        openStreamClick: (e, username) => {
            e.preventDefault()
            livestreamer.open_stream(username)
        },
        // this will be a logout button, but the behaviour at the moment is more like changing accounts
        // because it just asks you for a new account and logs into that immediately
        // also once requests and stuff are handled by actions, make sure to dispatch FetchFollowList after logging in is complete
        logoutClick: (e) => {
            e.preventDefault()
            twitch.logout()
        }
    }
}

let LiveListView = connect(mapStateToProps, mapDispatchToProps)(ListViewShape)
export default LiveListView
