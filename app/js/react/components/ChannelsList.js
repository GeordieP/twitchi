import React from 'react'
import Channel from './Channel'

module.exports = React.createClass({
    render() {
        return (
            <ul id="channelsList">
                {
                    this.props.liveChannels.map(channel =>
                        <Channel key={channel.username} channel={channel} openStreamClick={this.props.openStreamClick}/>
                    )
                }
            </ul>
        )
    }
})
