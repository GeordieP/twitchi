import React from 'react'

export default React.createClass({
    render() {
        return(
            <li onClick={(e) => this.props.openStreamClick(e, this.props.channel.username)} className="clickable">
                {/*<p>{this.props.channel.username} is playing {this.props.channel.game} for {this.props.channel.viewers} viewers</p>*/}
                <img src={this.props.channel.thumbnail_url} />
                <div>
                    <p>{this.props.channel.username}</p>
                    <p><span>{this.props.channel.game}</span></p>
                    <p>{this.props.channel.title}</p>
                    <p>{this.props.channel.viewers}</p>
                </div>
            </li>
        )
    }
})
