import React from 'react'
import { render } from 'react-dom'
import { Router, Route, hashHistory, Link } from 'react-router'
import MainListView from '../containers/MainListView'
import SettingsView from '../containers/SettingsView'

export default React.createClass({
    render() {
        return (
            <div id="frame">
                {/*window controls here*/}
                <main>
                    <Router history={hashHistory}>
                        <Link to={'/'}>Home</Link>
                        <Link to={'/settings'}>Settings</Link>
                        <div>
                            <Route path="/" component={MainListView} />
                            <Route path="/settings" component={SettingsView} />
                        </div>
                    </Router>
                </main>

            </div>
        )
    }
})
