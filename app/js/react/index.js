import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import AppMain from './components/AppMain'
import TwitchiReducers from './reducers'
import { FetchUsername, FetchFollowList } from './actions'
const twitch = window.twitch

let store = createStore(TwitchiReducers)

render (
    <Provider store={store}>
        <AppMain />
    </Provider>,
    document.getElementById('root')
)


/* Start Everything by first initializing the twitch interface */
twitch.init((init_success) => {
    if (!init_success) {
        console.error('error initializing twitch')
        return
    }

    // dispatch some actions to initially get data and update the views
    store.dispatch(FetchUsername(store.dispatch))
    store.dispatch(FetchFollowList(store.dispatch))
})
