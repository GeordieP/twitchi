'use strict'

var ipc = require('electron').ipcRenderer
var twitch = window.twitch

var update_ui = (streams_data) => {
    let streams = JSON.parse(streams_data).streams
    let stream
    let outputdiv = document.getElementById('output')
    outputdiv.innerHTML = ''

    for (let i = 0; i < streams.length; i++) {
        let stream = streams[i]
        outputdiv.innerHTML += '<p>' + stream.channel.name + ' is playing ' + stream.game + ' for ' + stream.viewers + ' viewers'
        outputdiv.innerHTML += '</p>'
    }
}
