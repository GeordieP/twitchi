'use strict'

let electron = require('electron')
let livestreamer = require('./livestreamer')
let BrowserWindow = electron.BrowserWindow
let ipc = electron.ipcMain

var traverse = function(obj) {
    for (var property in obj) {
        if (obj.hasOwnProperty(property)) {
            console.log("[TRV]: " + property);
        }
    }
}

module.exports = (conf) => {
    livestreamer.setup_quality_priority(conf)

    ipc.on('auth-fetch-access-token', (event) => {
        event.sender.send('auth-update-access-token', conf.get('auth_access_token'))
    })

    ipc.on('auth-clear-access-token', (event) => {
        conf.del('auth_access_token')
    })

    ipc.on('auth-refresh-access-token', (event, auth_url) => {
        conf.del('auth_access_token')
        var new_access_token = ''

        var authWindow = new BrowserWindow({
            width: 420,
            height: 600,
            'node-integration': false
        })

        authWindow.setMenu(null);
        // clear cookies and cache so the user doesn't get auto-logged in without a chance to enter credentials
        authWindow.webContents.session.clearStorageData([], () => {})

        authWindow.loadURL(auth_url)
        authWindow.webContents.on('did-get-redirect-request', (e, oldUrl, newUrl, isMainFrame) => {
            if (newUrl.indexOf('/oauthCallback#access_token=') > -1) {
                new_access_token = newUrl.split('#access_token=')[1]
                new_access_token = new_access_token.split('&scope=user_read')[0]

                conf.set('auth_access_token', new_access_token)
                event.sender.send('auth-update-access-token', conf.get('auth_access_token'))
                authWindow.close()
            }
        })
    })

    ipc.on('ls-open-stream', (event, username) => {
        livestreamer.open_stream(username, conf.get('auth_access_token'))
    })
}
