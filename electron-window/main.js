'use strict';

const configstore = require('configstore')
const pkg = require('../package.json')
const conf = new configstore(pkg.name)

// const {app, ipc, BrowserWindow} = require('electron')

const electron = require('electron')
const BrowserWindow = electron.BrowserWindow
const ipc = electron.ipcMain
const app = electron.app
let mainWindow

const ipchandler = require('./ipchandler')(conf)

var createWindow = () => {
	mainWindow = new BrowserWindow({
		width: 800,
		height: 600
	})


	
	/*--- Debug Stuff ---*/
	// mainWindow.webContents.openDevTools()
	// mainWindow.webContents.session.clearCache(() => {
	// 	console.log('should have cleared cache?')
	// })

    // mainWindow.webContents.session.clearStorageData([], function(data) {
    //     console.log('should have cleared storage data?')
    //     console.log(data)
    // })
	mainWindow.setMenu(null);

	mainWindow.loadURL('file://' + __dirname + '/../app/index.html')
	

	mainWindow.on('closed', () => {
		
		mainWindow = null
	})
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit()
	}
})

app.on('activate', () => {
	if (mainWindow === null) {
		createWindow()
	}
})
