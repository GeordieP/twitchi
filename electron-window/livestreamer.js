'use strict'

const spawn = require('child_process').spawn
const events = require('events')
const event = new events()


let livestreamer = {}
let ls_instances = {}

const default_quality_priority = [ 'source', '1080p60', '720p60', '720p', 'best', 'high', 'medium', 'low' ]

let quality_priority = []

let token = ''

event.on('bad-quality-option', (instance) => {
    let username = instance.username
    let current_quality_index = quality_priority.indexOf(instance.quality)
    let next_quality_index = current_quality_index + 1

    instance.kill()
    
    // delete the existing instance
    delete ls_instances[username]

    if (current_quality_index === -1 || next_quality_index > quality_priority.length) {
        console.log('No quality options in the quality priority list are valid for the selected stream')
        return
    }
    
    // create a new instance in its place
    ls_instances[username] = create_ls_instance(username, quality_priority[next_quality_index])
})

event.on('stream-end', (instance) => {
    instance.kill()
    delete ls_instances[instance.username]
})

const create_ls_instance = function(username, quality) {
    let that = this || {}
    
    that.username = username
    that.quality = quality

    that.process = spawn('livestreamer', ['twitch.tv/' + that.username, that.quality, '--twitch-oauth-token=' + token])
    that.process.stdout.on('data', (msg) => {
        console.log('\t' + that.username + '\n\t' + msg)

        if (msg.indexOf("Stream ended") > -1)
            event.emit('stream-end', that)

        // check if we've come across the invalid stream quality error message
        if (msg.indexOf("error: The specified stream(s) ") > -1)
            //  emit bad quality event, pass ourselves
            event.emit('bad-quality-option', that)
    })

    that.kill = () => {
        that.process.kill()
    }


    return that
}

livestreamer.open_stream = (username, new_token) => {
    // open stream > attempt open (takes index) > bad q event emitted from onstdout > bad q event listener > call attempt open with next index

    // if the token has changed, replace the one we have
    if (token !== new_token) token = new_token

    // we create a new livestreamer instance that spawns a livestreamer process. it will pipe its stdout to an internal
    // per-instance function that will check for certain stdout messages. when these messages happen, event listeners in
    // this file will be invoked. for example, if the stdout checker reads the error message we get when we try to open an
    // invalid stream quality, we will emit bad-quality-option, which will kill and delete the instance from the ls_instances
    // object, and create another instance in its place using the next quality string in the quality_priority array. This
    // will keep happening until a valid quality option is found, or until the priority list is exhausted at which point
    // we will stop trying to open the stream

    // spawn a new livestreamer instance and keep track of it
    ls_instances[username] = create_ls_instance(username, quality_priority[0])
}

livestreamer.setup_quality_priority = (conf) => {
    let priority = conf.get('stream_quality_option_priority_list')
    if (!priority || priority.length === 0) {
        conf.set('stream_quality_option_priority_list', default_quality_priority)
        quality_priority = default_quality_priority
    } else {
        quality_priority = priority
    }
}

livestreamer.close_stream = (username) => {
    // find ls_instances[username] and despawn the process
}


module.exports = livestreamer
